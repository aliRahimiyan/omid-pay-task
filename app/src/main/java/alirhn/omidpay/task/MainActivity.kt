package alirhn.omidpay.task

import alirhn.omidpay.task.navigation.ProductsNavGraph
import alirhn.omidpay.task.presentation.viewModel.ProductsListViewModel
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import alirhn.omidpay.task.ui.theme.OmidPayTaskTheme
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.ui.Modifier
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        val viewModel by viewModels<ProductsListViewModel>()
        setContent {
            OmidPayTaskTheme {
                ProductsNavGraph( modifier = Modifier.fillMaxSize())
            }
        }
    }
}
