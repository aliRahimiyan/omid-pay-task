package alirhn.omidpay.task

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class ProductsApp : Application() {
}