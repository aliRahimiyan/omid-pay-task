package alirhn.omidpay.task.apiService

import alirhn.omidpay.task.model.Product
import alirhn.omidpay.task.model.ProductResponse
import alirhn.omidpay.task.utils.Constants
import retrofit2.http.GET
import retrofit2.http.Path

interface ProductsApiService {

    @GET(Constants.PRODUCT_END_POINT)
    suspend fun getAllProducts() : List<Product>

    @GET(Constants.PRODUCT_END_POINT_ID)
    suspend fun getProductById(@Path("id") id : String) : Product
}