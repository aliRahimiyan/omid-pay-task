package alirhn.omidpay.task.baseClasses

data class BaseUiState<T>(
    val loading : Boolean = false,
    val errorMessage : String? = null,
    val data : T? = null
)
