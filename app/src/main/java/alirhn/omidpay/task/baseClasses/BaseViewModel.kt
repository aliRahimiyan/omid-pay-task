package alirhn.omidpay.task.baseClasses

import alirhn.omidpay.task.R
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

abstract class BaseViewModel<T> : ViewModel(){
   protected abstract val viewModelState : MutableStateFlow<T>

   val uiState by lazy {
       viewModelState.stateIn(
           scope = viewModelScope ,
           started = SharingStarted.Eagerly,
           initialValue = viewModelState.value
       )
   }

    protected fun<T> asyncAction(
        action : suspend () -> Flow<Result<T>> ,
        loading : () -> Unit = {} ,
        success : (T) -> Unit = {} ,
        failure : (String) -> Unit = {} ,
    ) {
        loading()
        viewModelScope.launch (Dispatchers.IO){
            action().collect{result->
                result.onSuccess {data->
                    success(data)
                }
                result.onFailure {throwable ->
                   if(throwable.message.isNullOrEmpty()){
                       failure("Error message is Empty")
                   }
                    else {
                       failure(throwable.message!!)
                    }
                }
            }
        }
    }
}