package alirhn.omidpay.task.di

import alirhn.omidpay.task.apiService.ProductsApiService
import alirhn.omidpay.task.repository.ProductRepository
import alirhn.omidpay.task.repository.ProductRepositoryImpl
import alirhn.omidpay.task.utils.Constants
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {

    companion object {
        @Provides
        @Singleton
        fun providesApiService(
            gsonConverterFactory: GsonConverterFactory
        ) : ProductsApiService{
            return Retrofit.Builder().
                    baseUrl(Constants.BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .build().create(ProductsApiService::class.java)
        }

        @Provides
        @Singleton
        fun providesGson() : GsonConverterFactory{
            return GsonConverterFactory.create()
        }
    }

    @Binds
   abstract fun provideProductRepository(productRepositoryImpl: ProductRepositoryImpl) : ProductRepository

}