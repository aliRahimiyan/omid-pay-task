package alirhn.omidpay.task.di

import alirhn.omidpay.task.local.AppDatabase
import alirhn.omidpay.task.local.ProductDao
import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object LocalModule {

    @Provides
    @Singleton
    fun provideRoomDataBase(@ApplicationContext context: Context)
    : AppDatabase {
        return Room.databaseBuilder(
            context = context,
            klass = AppDatabase::class.java,
            name = "product_room.db"
        ).build()
    }

    @Provides
    fun provideUserDao(appDatabase: AppDatabase): ProductDao {
        return appDatabase.ProductDao()
    }
}