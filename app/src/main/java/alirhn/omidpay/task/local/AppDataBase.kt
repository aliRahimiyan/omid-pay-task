package alirhn.omidpay.task.local

import alirhn.omidpay.task.model.Product
import androidx.room.Database
import androidx.room.RoomDatabase


@Database(entities = [Product::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun ProductDao(): ProductDao
}