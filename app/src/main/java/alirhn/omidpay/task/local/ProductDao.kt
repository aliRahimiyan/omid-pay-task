package alirhn.omidpay.task.local

import alirhn.omidpay.task.model.Product
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ProductDao {

        @Query("SELECT * FROM product")
        fun getAll(): List<Product>

        @Query("SELECT * FROM product WHERE id IN (:productId) LIMIT 1")
        fun getById(productId: String): Product?

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        fun insert(productEntity: Product)

        @Delete
        fun delete(productEntity: Product)

}