package alirhn.omidpay.task.model

import com.google.gson.annotations.SerializedName

data class ProductResponse(
    val response: List<Product>
)
