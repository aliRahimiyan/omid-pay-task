package alirhn.omidpay.task.navigation

import alirhn.omidpay.task.presentation.viewModel.ProductDetailViewModel
import alirhn.omidpay.task.presentation.viewModel.ProductsListViewModel
import alirhn.omidpay.task.ui.composabales.MainProductsList
import alirhn.omidpay.task.ui.composabales.ProductDetailScreen
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Observer
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.CoroutineScope


@Composable
fun ProductsNavGraph(
    modifier: Modifier = Modifier,
    navController: NavHostController = rememberNavController(),
    coroutineScope: CoroutineScope = rememberCoroutineScope(),
    startDestination: String = Destinations.PRODUCTS_LIST,
    navAction: ProductsNavigation = remember(navController) {
        ProductsNavigation(navController)
    }
) {
    NavHost(
        modifier = modifier,
        navController = navController,
        startDestination = startDestination,
        contentAlignment = Alignment.Center
    ) {

        productsListScreen(
            builder = this,
            navAction = navAction,
        )

        productDetailsScreen(
            builder = this,
            navAction = navAction,
            navController = navController
        )

    }

}

fun productsListScreen(
    builder: NavGraphBuilder,
    navAction: ProductsNavigation,
) {
    builder.apply {
        composable(
            route = Destinations.PRODUCTS_LIST
        ) { entry ->
            val viewModel = hiltViewModel<ProductsListViewModel>()
            val state by viewModel.uiState.collectAsState()

            MainProductsList(
                navigateToNextScreen = { productId ->
                    navAction.navigateToProductDetail(productId)
                },
                uiState = state
            )
        }
    }
}

fun productDetailsScreen(
    builder: NavGraphBuilder,
    navAction: ProductsNavigation,
    navController: NavHostController
) {
    builder.apply {
        composable(
            route = Destinations.PRODUCTS_DETAIL_ROUTE,
        ) { entry ->

            val viewModel = hiltViewModel<ProductDetailViewModel>()
            val state by viewModel.uiState.collectAsState()

            ProductDetailScreen(
                uiState = state,
                onBackClicked = {
                    navController.popBackStack()
                    navAction.navigateToList()
                },
                onSaveClick = {
                    viewModel.saveToFavorites(it)
                },
                onDeleteProduct = {
                    viewModel.deleteProduct(it)
                },
                isLiked =  state.isChecked
            )
        }
    }
}
