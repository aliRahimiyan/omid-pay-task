package alirhn.omidpay.task.navigation
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
class ProductsNavigation (
    private val navController: NavHostController
){

        fun navigateToProductDetail(productId : String) {
            navController.navigate(
                route = "${Routes.PRODUCT_DETAIL}/$productId"
            ){
                this.popUpTo(id = navController.graph.findStartDestination().id) {
                    inclusive = true
                    saveState = true
                }
                launchSingleTop = true
            }
        }
    fun navigateToList() {
        navController.navigate(
            route = Routes.PRODUCT_LIST
        )
    }

    }