package alirhn.omidpay.task.navigation

object Routes {
    const val PRODUCT_DETAIL = "product_detail"
    const val PRODUCT_LIST = "product_list"
}

object Destinations {
    const val PRODUCTS_LIST = Routes.PRODUCT_LIST
    const val PRODUCTS_DETAIL_ROUTE =
        "${Routes.PRODUCT_DETAIL}/{${ProductsArg.PRODUCT_ARG}}"
}

object ProductsArg {
    const val PRODUCT_ARG = "id"
}