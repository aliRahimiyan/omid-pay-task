package alirhn.omidpay.task.presentation.uiState

import alirhn.omidpay.task.baseClasses.BaseUiState
import alirhn.omidpay.task.model.Product

data class ProductUIState (
    val productUIState : BaseUiState<Product> = BaseUiState(),
    val isChecked : Boolean = false
)