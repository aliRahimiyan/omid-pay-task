package alirhn.omidpay.task.presentation.uiState

import alirhn.omidpay.task.baseClasses.BaseUiState
import alirhn.omidpay.task.model.Product

data class ProductsListUIState (
    val productsListUIState : BaseUiState<List<Product>> = BaseUiState()
)