package alirhn.omidpay.task.presentation.viewModel

import alirhn.omidpay.task.baseClasses.BaseViewModel
import alirhn.omidpay.task.model.Product
import alirhn.omidpay.task.navigation.ProductsArg
import alirhn.omidpay.task.presentation.uiState.ProductUIState
import alirhn.omidpay.task.repository.ProductRepository
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductDetailViewModel @Inject constructor(
    private val repository: ProductRepository,
    private val savedStateHandle: SavedStateHandle,
) : BaseViewModel<ProductUIState>() {
    override val viewModelState: MutableStateFlow<ProductUIState> =
        MutableStateFlow(ProductUIState())
    private val productId: String? = savedStateHandle[ProductsArg.PRODUCT_ARG]

    init {
        productId?.let {
            getProductDetails(productId = productId)
            checkProductIsLiked()
        }
    }

     fun saveToFavorites(product: Product){
        viewModelScope.launch (Dispatchers.IO){
            repository.saveProduct(product)
        }
    }

     fun deleteProduct(product: Product){
        viewModelScope.launch(Dispatchers.IO){
            repository.deleteProduct(product)
        }
    }

    fun checkProductIsLiked(){
        viewModelScope.launch(Dispatchers.IO) {
            if(productId != null) {
                val result = repository.checkProductIsLiked(productId)
                updateProductLikeStatus(result)
            }
        }
    }
    private fun getProductDetails(productId: String) {
        asyncAction(
            action = { repository.getProductByID(id = productId) },
            loading = {
                onLoadingState(isLoading = true)
            },
            success = {
                onSuccessState(it)
            },
            failure = {
                onFailure(it)
            },
        )
    }

    private fun updateProductLikeStatus(isLiked : Boolean){
        viewModelState.update {
            it.copy(
                isChecked =isLiked
            )
        }
    }

    private fun onLoadingState(isLoading: Boolean) {
        updateLoadingState(isLoading = true)
        updateSuccessState(data = null)
        updateFailureState(errorMessage = null)
    }


    private fun onSuccessState(data: Product?) {
        updateLoadingState(isLoading = false)
        updateSuccessState(data = data)
        updateFailureState(errorMessage = null)
    }

    private fun onFailure(errorMessage: String?) {
        updateLoadingState(isLoading = false)
        updateSuccessState(data = null)
        updateFailureState(errorMessage = errorMessage)
    }

    private fun updateLoadingState(isLoading: Boolean) {
        viewModelState.update {
            it.copy(
                productUIState = it.productUIState.copy(loading = isLoading)
            )
        }
    }

    private fun updateSuccessState(data: Product?) {
        viewModelState.update {
            it.copy(
                productUIState = it.productUIState.copy(
                    data = data
                )
            )
        }
    }

    private fun updateFailureState(errorMessage: String?) {
        viewModelState.update {
            it.copy(
                productUIState = it.productUIState.copy(
                    errorMessage = errorMessage
                )
            )
        }
    }
}