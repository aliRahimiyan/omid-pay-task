package alirhn.omidpay.task.presentation.viewModel

import alirhn.omidpay.task.baseClasses.BaseViewModel
import alirhn.omidpay.task.model.Product
import alirhn.omidpay.task.presentation.uiState.ProductsListUIState
import alirhn.omidpay.task.repository.ProductRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class ProductsListViewModel @Inject constructor(
    private val repository: ProductRepository
) : BaseViewModel<ProductsListUIState>() {
    override val viewModelState: MutableStateFlow<ProductsListUIState> =
        MutableStateFlow(ProductsListUIState())


    init {
        getAllPosts()
    }

    private fun getAllPosts() {
        asyncAction(
            action = { repository.getAllProducts() },
            loading = {
                onLoadingState()
            },
            success = {
                onSuccessState(data = it)
            },
            failure = {
                onFailureState(it)
            },
        )
    }

    private fun onLoadingState() {
        updateLoadingState(true)
        updateSuccessState(data = null)
        updateFailureState(errorMessage = null)
    }

    private fun onSuccessState(data: List<Product>) {
        updateLoadingState(isLoading = false)
        updateSuccessState(data = data.sortedBy {
            it.price
        })
        updateFailureState(errorMessage = null)
    }

    private fun onFailureState(errorMessage: String) {
        updateLoadingState(isLoading = false)
        updateSuccessState(data = null)
        updateFailureState(errorMessage = errorMessage)
    }

    private fun updateLoadingState(isLoading: Boolean) {
        viewModelState.update { uiState ->
            uiState.copy(
                productsListUIState = uiState.productsListUIState.copy(loading = isLoading)
            )
        }
    }

    private fun updateSuccessState(data: List<Product>?) {
        viewModelState.update { uiState ->
            uiState.copy(
                productsListUIState = uiState.productsListUIState.copy(data = data)
            )
        }
    }

    private fun updateFailureState(errorMessage: String?) {
        viewModelState.update { uiState ->
            uiState.copy(
                productsListUIState = uiState.productsListUIState.copy(errorMessage = errorMessage)
            )
        }
    }
}