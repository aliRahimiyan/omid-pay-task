package alirhn.omidpay.task.repository

import alirhn.omidpay.task.model.Product
import kotlinx.coroutines.flow.Flow

interface ProductRepository  {
    suspend fun getAllProducts() : Flow<Result<List<Product>>>

    suspend fun getProductByID(id : String) : Flow<Result<Product>>

    suspend fun saveProduct(product: Product)

    suspend fun getAllSaved() : Flow<Result<List<Product>>>

    suspend fun deleteProduct(product: Product)

    suspend fun checkProductIsLiked(id  : String) : Boolean
}