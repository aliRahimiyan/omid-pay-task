package alirhn.omidpay.task.repository

import alirhn.omidpay.task.apiService.ProductsApiService
import alirhn.omidpay.task.local.ProductDao
import alirhn.omidpay.task.model.Product
import alirhn.omidpay.task.utils.apiCall
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class ProductRepositoryImpl @Inject constructor(
    private val productsApiService: ProductsApiService,
    private val productDao: ProductDao
)  : ProductRepository{
    override suspend fun getAllProducts(): Flow<Result<List<Product>>>  = flow {
       val result = apiCall { productsApiService.getAllProducts() }
        result.onSuccess {productResponse ->
            emit(Result.success(productResponse))
        }
        result.onFailure {
            emit(Result.failure(it))
        }
    }

    override suspend fun getProductByID(id: String): Flow<Result<Product>> = flow {
        val result = apiCall { productsApiService.getProductById(id = id) }
        result.onSuccess {product->
            emit(Result.success(product))
        }
        result.onFailure {
            emit(Result.failure(it))
        }
    }

    override suspend fun saveProduct(product: Product) {
        productDao.insert(productEntity = product)
    }

    override suspend fun getAllSaved(): Flow<Result<List<Product>>>  = flow {
        val result = runCatching { productDao.getAll() }

        result.onSuccess {
            emit(Result.success(it))
        }

        result.onFailure {
            emit(Result.failure(it))
        }
    }

    override suspend fun deleteProduct(product: Product) {
        productDao.delete(product)
    }

    override suspend fun checkProductIsLiked(id : String) : Boolean{
        val result = productDao.getById(id)
        return result != null
    }


}