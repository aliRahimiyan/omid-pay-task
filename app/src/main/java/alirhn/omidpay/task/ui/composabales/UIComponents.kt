package alirhn.omidpay.task.ui.composabales

import alirhn.omidpay.task.R
import alirhn.omidpay.task.model.Product
import alirhn.omidpay.task.presentation.uiState.ProductUIState
import alirhn.omidpay.task.presentation.uiState.ProductsListUIState
import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Snackbar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import coil.compose.AsyncImage
import kotlinx.coroutines.launch
@Composable
fun MainProductsList(
    modifier: Modifier = Modifier,
    uiState: ProductsListUIState,
    navigateToNextScreen: (String) -> Unit,
) {

    val isLoading by remember(uiState.productsListUIState.loading) {
        derivedStateOf {
            uiState.productsListUIState.loading
        }
    }

    val isSuccess by remember(uiState.productsListUIState.data) {
        derivedStateOf {
            uiState.productsListUIState.data != null
        }
    }

    val isFailed by remember(uiState.productsListUIState.errorMessage) {
        derivedStateOf {
            uiState.productsListUIState.errorMessage != null
        }
    }

    when {
        isLoading -> ProductDefaultLoading()
        isSuccess -> {
            uiState.productsListUIState.data?.let {
                ProductsList(
                    products = uiState.productsListUIState.data,
                    navigateToNextScreen = navigateToNextScreen
                )
            }
        }

        isFailed -> {
            uiState.productsListUIState.errorMessage?.let {
                Text(text = it, color = Color.Red, fontWeight = FontWeight.Bold)
            }
        }
    }
}

@Composable
fun ProductsList(
    modifier: Modifier = Modifier,
    products: List<Product>,
    navigateToNextScreen: (String) -> Unit
) {
    LazyColumn(modifier = modifier.padding(top = 16.dp)) {
        items(count = products.size) {
            ProductItem(product = products.get(it), navigateToNextScreen = navigateToNextScreen)
        }
    }
}

@Composable
fun ProductItem(
    modifier: Modifier = Modifier,
    product: Product,
    navigateToNextScreen: (String) -> Unit
) {
    Column(modifier = modifier.clickable {
        navigateToNextScreen(product.id.toString())
    }) {
        AsyncImage(
            model = product.image,
            contentDescription = "",
            modifier = Modifier
                .fillMaxWidth()
                .padding(all = 16.dp)
                .height(160.dp)
        )

        Spacer(modifier = Modifier.height(16.dp))

        ProductFieldsRow(imageVector = Icons.Filled.Info, fieldValue = product.title)

        Spacer(modifier = Modifier.height(16.dp))

        ProductFieldsRow(
            imageVector = Icons.Default.ShoppingCart,
            fieldValue = product.price.toString()
        )

        Spacer(modifier = Modifier.height(16.dp))

        ProductFieldsRow(imageVector = Icons.Filled.Menu, fieldValue = product.category)

        Spacer(modifier = modifier.height(16.dp))

        Divider()
    }
}

@Composable
fun ProductFieldsRow(modifier: Modifier = Modifier, imageVector: ImageVector, fieldValue: String) {
    ConstraintLayout {
        val (icon, value) = createRefs()
        Icon(imageVector = imageVector, contentDescription = "",
            modifier
                .size(20.dp)
                .constrainAs(
                    icon
                ) {
                    start.linkTo(parent.start, margin = 16.dp)
                })
        Text(
            text = fieldValue.take(20),
            modifier = modifier
                .constrainAs(
                    value
                ) {
                    start.linkTo(icon.end, margin = 16.dp)

                }, maxLines = 1
        )
    }
}

@Composable
fun ProductDetailScreen(
    modifier: Modifier = Modifier,
    uiState: ProductUIState,
    onBackClicked: () -> Unit,
    onSaveClick: (Product) -> Unit,
    onDeleteProduct: (Product) -> Unit,
    isLiked: Boolean

) {

    BackHandler {
        onBackClicked()

    }
    val isLoading by remember(uiState.productUIState.loading) {
        derivedStateOf {
            uiState.productUIState.loading
        }
    }

    val isSuccess by remember(uiState.productUIState.data) {
        derivedStateOf {
            uiState.productUIState.data != null
        }
    }

    val isFailure by remember(uiState.productUIState.errorMessage) {
        derivedStateOf {
            uiState.productUIState.errorMessage != null
        }
    }

    when {
        isLoading -> ProductDefaultLoading()
        isSuccess -> {
            uiState.productUIState.data?.let {
                ProductDetail(
                    it,
                    onSaveClick = onSaveClick,
                    onDeleteProduct = onDeleteProduct,
                    isLiked = isLiked
                )
            }
        }

        isFailure -> {
            uiState.productUIState.errorMessage?.let {
                Text(text = it, color = Color.Red, fontWeight = FontWeight.Bold)
            }
        }
    }
}

@Composable
fun ProductDetail(
    product: Product, modifier: Modifier = Modifier,
    onSaveClick: (Product) -> Unit, onDeleteProduct: (Product) -> Unit, isLiked: Boolean
) {

    Column(
        modifier = modifier.padding(top = 48.dp, end = 16.dp, start = 16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        AsyncImage(model = product.image, contentDescription = "")

        Spacer(modifier = modifier.height(20.dp))

        Text(text = product.title, fontWeight = FontWeight.W300, fontSize = 14.sp)

        Spacer(modifier = modifier.height(16.dp))

        ProductDetailsBox(
            product = product,
            onSaveClick = onSaveClick,
            onDeleteProduct = onDeleteProduct,
            isLiked = isLiked
        )
    }
}

@Composable
fun ProductDetailsBox(
    modifier: Modifier = Modifier, product: Product, onSaveClick: (Product) -> Unit,
    onDeleteProduct: (Product) -> Unit, isLiked: Boolean
) {

    val scope = rememberCoroutineScope()
    val context = LocalContext.current

    var liked by remember {
        mutableStateOf(isLiked)
    }
    Column(
        Modifier
            .background(
                shape = RoundedCornerShape(corner = CornerSize(size = 12.dp)),
                color = Color.LightGray
            )
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = modifier
                .fillMaxWidth()
                .padding(all = 8.dp)
        ) {
            Text(text = "price : ${product.price} $ ")
            Icon(
                imageVector = if (liked) Icons.Filled.Favorite else Icons.Default.FavoriteBorder,
                contentDescription = "",
                modifier = modifier.clickable {
                    liked = !liked
                    if (liked) {
                        onSaveClick(product)
                        scope.launch {
                            Toast.makeText(context , "Saved" , Toast.LENGTH_SHORT).show()
                        }

                    } else {
                        onDeleteProduct(product)
                        scope.launch {

                            Toast.makeText(context , "deleted" , Toast.LENGTH_SHORT).show()
                        }
                    }
                },
                tint = if (liked) Color.Red else Color.Black
            )
        }
        Spacer(modifier = modifier.height(16.dp))
        Text(
            text = "Category : ${product.category}",
            fontWeight = FontWeight.W300,
            fontSize = 22.sp,
            modifier = modifier.padding(all = 8.dp)
        )
        Spacer(modifier = modifier.height(16.dp))
        Text(
            text = stringResource(id = R.string.descriptions),
            fontWeight = FontWeight.ExtraBold,
            color = Color.Blue,
            modifier = modifier.padding(all = 8.dp)
        )
        Divider()
        Spacer(modifier = modifier.height(8.dp))
        Text(
            text = product.description,
            fontWeight = FontWeight.W300,
            fontSize = 18.sp,
            modifier = modifier.padding(all = 8.dp)
        )
    }
}

@Composable
fun ProductDefaultLoading() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colorScheme.background),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Text(
            text = stringResource(id = R.string.please_wait),
            color = MaterialTheme.colorScheme.onBackground,
            fontSize = 24.sp
        )

    }
}
