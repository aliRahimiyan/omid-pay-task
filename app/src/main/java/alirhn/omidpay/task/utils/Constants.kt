package alirhn.omidpay.task.utils

object Constants {
    const val BASE_URL = "https://fakestoreapi.com"
    const val PRODUCT_END_POINT = "/products"
    const val PRODUCT_END_POINT_ID = "/products/{id}"
}