package alirhn.omidpay.task.utils

inline fun <T> apiCall(action : () -> T) : Result<T>{
    return runCatching { action() }
}